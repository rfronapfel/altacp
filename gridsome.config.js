const path = require('path');

function addStyleResource(rule) {
  rule
    .use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [path.resolve(__dirname, './src/assets/*.scss')]
    });
}

module.exports = {
  siteName: 'Alta Capital Partners',
  siteDescription: 'Alta Capital Partners is a leading boutique advisory firm providing best-in-class investment banking services to middle market clients around the world',
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'src/pages/index.md',
        typeName: 'Frontpage',
        route: '/'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'src/pages/transactions/*.md',
        typeName: 'Transactions',
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'src/pages/industries/*.md',
        typeName: 'Industries',
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'src/pages/partners/*.md',
        typeName: 'Partners',
        route: '/partners/:slug',
      }
    },
    {
      use: `gridsome-plugin-netlify-cms`,
      options: {
        configPath: `static/admin/config.yml`,
        htmlPath: `static/admin/index.html`,
        publicPath: `/admin`,
        htmlTitle: `My CMS`,
        plugins: [`netlify-cms-widget-color`]
      }
    },
    {
      use: 'gridsome-plugin-netlify-cms-paths',
      options: {
        contentTypes: ['Frontpage', 'Partners', 'Transactions', 'Industries'],
        coverField: 'cover_image'
      }
    }
  ],
  transformers: {
    remark: {
      plugins: [
        '@gridsome/remark-prismjs'
      ]
    }
  },
  chainWebpack: config => {
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
    types.forEach(type =>
      addStyleResource(config.module.rule('scss').oneOf(type))
    );
  }
}
