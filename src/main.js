require('typeface-roboto')
import DefaultLayout from '~/layouts/Default.vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { config, library } from '@fortawesome/fontawesome-svg-core'
import { faLinkedin, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope, faBars, faAd } from '@fortawesome/free-solid-svg-icons'
import '@fortawesome/fontawesome-svg-core/styles.css'
import AOS from 'aos';
import 'aos/dist/aos.css';
import 'prismjs/themes/prism.css'

config.autoAddCss = false;
library.add(faLinkedin, faEnvelope, faLinkedinIn, faBars, faAd)

export default function (Vue, { router, head, isClient }) {
  Vue.component('Layout', DefaultLayout)
  Vue.component('font-awesome', FontAwesomeIcon);

  if (process.isClient) {
    Vue.use(AOS.init());
  };
}