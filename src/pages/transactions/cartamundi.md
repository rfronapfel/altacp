---
title: >-
  Advised Cartamundi on the acquisition of The United States Playing Card
  Company
text: >-
  Cartamundi is the world’s leading manufacturer of card and board games and
  digital solutions. We advised the family-owned Belgian company on the
  acquisition of The United States Playing Card Company, owner of iconic brands
  such as Bicycle® and Bee®, and the global standard within the world of playing
  cards, from Newell Brands. Through this acquisition, Cartamundi expands its
  U.S. presence, increases its brand portfolio, and gains access to new
  customers.
cover_image: /uploads/transactions-1.jpg
logo: /uploads/transactions-1-logos.jpg
date: 2019-01-03
---

