---
title: 'Advised Hubei Huachangda on the acquisition of Dearborn Mid-West '
text: >-
  Hubei Huachangda is a publicly-traded Chinese company engaged in the design,
  manufacture, and installation of intelligent automation equipment systems.
  With the acquisition of Dearborn Mid-West, a leading material handling
  solutions provider to the North American auto industry, it gained strategic
  customer relationships and strong engineering / project management
  capabilities.
cover_image: /uploads/transactions-3.jpg
logo: /uploads/transactions-3-logos.jpg
date: 2019-01-01
---

