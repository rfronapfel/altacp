---
title: "Advised Quest Aircraft on its sale to Setouchi"
text: "Quest designs and manufactures the Kodiak, a rugged single engine turboprop plane capable of short takeoff and landing that was originally designed for humanitarian aid. We advised Quest on a dual track M&A and financing process that resulted in a successful cross-border sale to Setouchi, a member of Tsuneishi Group, a Japanese global shipbuilder and transportation provider."
cover_image: /uploads/transactions-2.jpg
logo: /uploads/transactions-2-logos.jpg
date: 2019-01-02
---