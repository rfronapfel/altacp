---
title: 'Served as Placement Agent for Abpro’s Series B Financing'
text: >-
  Abpro is a biotechnology company dedicated to developing  next-generation
  antibody therapeutics to improve the lives of patients with severe and
  life-threatening diseases. At a critical juncture, we helped raise its Series
  B financing on an accelerated basis to help the company continue its rapid
  growth and expansion.
cover_image: /uploads/transactions-4.jpg
logo: /uploads/transactions-4-logos.jpg
date: 2018-01-04
---

