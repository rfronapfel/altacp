---
title: Mark Tashkovich
image: /uploads/mark.jpg
position: Founding Partner
mail: mailto:mark.tashkovich@altacp.com
linkedin: https://www.linkedin.com/in/mark-tashkovich-965241/
experience_1: Morgan Stanley
experience_2: UBS Investment Bank
experience_3: Ashir Capital
date: 2019-01-03
---
#### Biography

Mark Tashkovich is the CEO and a Managing Director of Alta Capital Partners and has significant experience advising on domestic and cross-border M&A, equity, and debt transactions in the consumer/retail, industrial, healthcare, telecom, technology, and alternative energy sectors.  

Prior to Alta, Mark served as the CEO of Ashir Capital, a 75-person, New York-based, China-focused investment bank with offices in Beijing, Guangzhou, and Hong Kong.  In that role, Mark led global strategy and operations and oversaw origination and execution of the firm’s transactions. 

Mark previously served as a senior coverage officer in the Global Consumer Products & Retail Group at UBS Investment Bank. In that role, Mark built UBS's investment banking franchise in several specialty retail subsectors and coordinated global coverage with teams in Asia, Europe, and Latin America. 

Before that, Mark worked in Equity Capital Markets at UBS and Morgan Stanley. During those years, Mark helped lead the execution of over 100 equity, equity-linked, and high yield financings, raising over $30 billion.  At Morgan Stanley, Mark also partnered in originating, structuring, and syndicating acquisition financings and bridges for over $22 billion. 

Prior to Morgan Stanley, Mark served as a corporate attorney at Weil, Gotshal & Manges, and clerked for the Chief Judge of the Federal District Court of Hawaii. 

Mark holds a JD from the University of Pennsylvania, where he was elected Articles Editor of the Law Review, an MPhil from the University of Cambridge, and a BA cum laude from Cornell University.

Additionally, Mark serves as a strategic advisor to American Ballet Theatre and as a board member for the Sorensen Center for International Peace and Justice.
