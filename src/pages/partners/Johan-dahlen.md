---
title: Johan DahlÉn
image: /uploads/johan.jpg
position: Founding Partner
mail: mailto:johan.dahlen@altacp.com
linkedin: https://www.linkedin.com/in/johan-dahlen-823a13b/
experience_1: Morgan Stanley
experience_2: UBS Investment Bank
experience_3: Jefferies
date: 2019-01-02
---

#### Biography

Johan Dahlén is a Managing Director of Alta Capital Partners and has significant investment banking experience in domestic and international M&A as well as equity and debt financings. He has advised clients in emerging and developed markets in several industries including industrials, chemicals, alternative energy, telecom, technology, and consumer/retail.

Prior to Alta, Johan worked at Morgan Stanley, UBS Investment Bank, and Jefferies, where most recently he was the senior coverage officer focused on industrial technology covering clients including General Electric, United Technologies, Eaton, TE Connectivity, Sensata, Amphenol, ABB, and Schneider Electric.

Over his career, Johan has executed M&A transactions totaling over $23 billion. Notable M&A deals include GE Advanced Materials/Apollo, Telia’s Tess/Telecom Americas, SBC’s Ameritech New Media/Wide Open West, Bestfoods/Arisco, BBVA/Bancomer, International Paper/WY Containerboard, Stora Enso NA/Newpage, and Anchor Glass/Ardagh.

Additionally, Johan has executed financings that have raised over $13 billion. Notable transactions include a private placement for Eclipse Aviation; initial public offerings for Embraer and Petrotec; and debt financings for Federative Republic of Brazil, International Paper, and NewPage.

Johan received both his Bachelor of Science and Master of Science degrees in economics from the Stockholm School of Economics. He is fluent in Swedish and Portuguese, conversant in Spanish, and has lived in Sweden and Brazil.
