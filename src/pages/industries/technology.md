---
title: Technology
text: ''
cover_image: /uploads/technology.jpg
bg_color: "#AC50D5"
date: 2019-01-04
---

- Communications
- SaaS / DaaS
- Data / analytics
- Artificial intelligence