---
title: Consumer & Retail
text: ''
cover_image: /uploads/consumer_retail.jpg
bg_color: "#DB0000"
date: 2019-01-01
---

- Healthy and active living
- Personal care and beauty
- Food and beverage
- Apparel and footwear