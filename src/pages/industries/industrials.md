---
title: Industrials
text: ''
cover_image: /uploads/industrials.jpg
bg_color: "#008EDB"
date: 2019-01-02
---

- Industrial technology
- Logistics
- Basic materials
- Alternative energy