---
title: Healthcare
text: ''
cover_image: /uploads/healthcare.jpg
bg_color: "#008217"
date: 2019-01-03
---

- Medical devices
- Diagnostics
- Biotechnology
- Specialty pharma